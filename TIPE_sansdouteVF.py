import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import matplotlib.image as mpimg
import pylab
import sys
sys.setrecursionlimit(1000)

obstacle = 99 # Valeur numérique dans la matrice représentative de la pièce pour représenter un obstacle
MatTest1 = [[1       ,1       ,1 ,1       ,1       ,1       ,1       ,1  ,1      ,1       ],
            [obstacle,obstacle,1 ,obstacle,obstacle,obstacle,obstacle,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1       ],
            [obstacle,obstacle,1 ,obstacle,obstacle,obstacle,obstacle,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1       ],
            [obstacle,obstacle,1 ,obstacle,obstacle,obstacle,obstacle,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1       ],
            [obstacle,obstacle,1 ,obstacle,obstacle,obstacle,obstacle,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1       ],
            [obstacle,obstacle,1 ,obstacle,obstacle,obstacle,obstacle,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1       ],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1       ]]

I211 = [[1       ,1       ,1 ,1,1      ,1       ,1       ,1       ,1  ,1,1      ,1       ],
            [obstacle,obstacle,1,1 ,obstacle,obstacle,obstacle,obstacle,1,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1,1,1       ,1       ,1       ,1       ,1 ,1       ,1       ],
            [obstacle,obstacle,1,1 ,obstacle,obstacle,obstacle,obstacle,1,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1 ,1,1      ,1       ],
            [obstacle,obstacle,1,1 ,obstacle,obstacle,obstacle,obstacle,1 ,1,obstacle,obstacle],
            [1       ,1       ,1 ,1   ,1,1    ,1       ,1       ,1       ,1 ,1       ,1       ],
            [obstacle,obstacle,1,1 ,obstacle,obstacle,obstacle,obstacle,1,1 ,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1  ,1,1     ,1       ],
            [obstacle,obstacle,1 ,1,obstacle,obstacle,obstacle,obstacle,1 ,1,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1  ,1,1     ,1       ],
            [obstacle,obstacle,1 ,1,obstacle,obstacle,obstacle,obstacle,1 ,1,obstacle,obstacle],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1 ,1,1      ],
            [1       ,1       ,1 ,1       ,1       ,1       ,1       ,1 ,1       ,1 ,1,1      ]]

PersonnesI211 = [[(2, 0), True, True], [(2, 1), True, True], [(2, 4), True, True], [(2, 5), True, True], [(2, 6), True, True], [(2, 7), True, True], [(2, 10), True, True], [(2,11), True, True], [(4, 0), True, True], [(4, 1), True, True], [(4, 4), True, True], [(4, 5), True, True], [(4, 6), True, True], [(4, 7), True, True], [(4, 10),True, True], [(4, 11), True, True], [(6, 0), True, True], [(6, 1), True, True],[(6, 4), True, True], [(6, 5), True, True], [(6, 6), True, True], [(6, 7), True, True], [(6, 10), True, True], [(6, 11), True, True], [(8, 0), True, True], [(8, 1), True, True], [(8, 4), True, True], [(8, 5), True, True], [(8, 6), True, True], [(8, 7), True, True], [(8, 10), True, True], [(8, 11), True, True], [(10, 0), True, True], [(10, 1), True, True], [(10, 4), True, True], [(10, 5), True, True], [(10, 6), True, True], [(10, 7), True, True], [(10, 10), True, True], [(10,11), True, True], [(12, 0), True, True], [(12, 1), True, True], [(12, 4), True,True], [(12, 5), True, True], [(12, 6), True, True], [(12, 7), True, True], [(12, 10), True, True], [(12, 11), True, True]]
            
PersonnesTest1 = [[(2,0),True,True],[(2,1),True,True],[(2,3),True,True],[(2,4),True,True],[(2,5),True,True],[(2,6),True,True],[(2,8),True,True],[(2,9),True,True],
                  [(4,0),True,True],[(4,1),True,True],[(4,3),True,True],[(4,4),True,True],[(4,5),True,True],[(4,6),True,True],[(4,8),True,True],[(4,9),True,True],
                  [(6,0),True,True],[(6,1),True,True],[(6,3),True,True],[(6,4),True,True],[(6,5),True,True],[(6,6),True,True],[(6,8),True,True],[(6,9),True,True],
                  [(8,0),True,True],[(8,1),True,True],[(8,3),True,True],[(8,4),True,True],[(8,5),True,True],[(8,6),True,True],[(8,8),True,True],[(8,9),True,True],
                  [(10,0),True,True],[(10,1),True,True],[(10,3),True,True],[(10,4),True,True],[(10,5),True,True],[(10,6),True,True],[(10,8),True,True],[(10,9),True,True]]
                  
PersonnesTest2 = [[(2,0),True,True],[(2,2),True,True],[(2,3),True,True],[(2,4),True,True],[(2,5),True,True],[(2,6),True,True],[(2,8),True,True],[(2,9),True,True],
                  [(4,0),True,True],[(4,1),True,True],[(4,3),True,True],[(4,4),True,True],[(4,5),True,True],[(4,6),True,True],[(4,8),True,True],[(4,9),True,True],
                  [(6,0),True,True],[(6,1),True,True],[(6,3),True,True],[(6,4),True,True],[(6,5),True,True],[(6,6),True,True],[(6,8),True,True],[(6,9),True,True],
                  [(8,0),True,True],[(8,1),True,True],[(8,3),True,True],[(8,4),True,True],[(8,5),True,True],[(8,6),True,True],[(8,8),True,True],[(8,9),True,True],
                  [(10,0),True,True],[(10,1),True,True],[(10,3),True,True],[(10,4),True,True],[(10,5),True,True],[(10,6),True,True],[(10,8),True,True],[(10,9),True,True]]
# Chaque personne est représentée par un tableau du tableau : la première case représente la position, la deuxième indique si la personne s'est déplacée ce période de temps-ci (False) ou pas encore (True), la troisième indique si la personne est toujours dans la matrice (True) ou si elle est sortie (False)



def matDep(M,Sortie) : 
# On lui donne la matrice de la pièce et les coordonnées de la sortie et elle renvoie la matrice des déplacements (pour chaque case libre, elle associe une case vers laquelle la personne doit se diriger), la matrice de distance (à combien de cases de la sortie cette case est) et le tableau des cases n'ayant pas accès à la sortie.
    xs,ys = Sortie  # Coordonnées de la sortie
    hauteur = len(M)
    largeur = len(M[0])
    nombreCases = hauteur * largeur
    MatriceDistance = [[-1 for j in range(largeur)]for i in range(hauteur)] 
    # La matrice sera remplie au fur et à mesure de la fonction, et elle indique, pour chaque case, combien de cases libres la sépare de la sortie
    MatriceDistance[xs][ys] = 0
    casesRemplies = 1
    # Les cases adjacentes à la sortie, et libres, sont à 1 case de la sortie :
    CasesPre = []
    if xs-1 >= 0 : # On vérifie que la case existe bien
        casesRemplies += 1
        if M[xs-1][ys] != obstacle : # Si cette case ne représente pas un obstacle
            MatriceDistance[xs-1][ys] = 1
            CasesPre.append((xs-1,ys))
        else : MatriceDistance[xs-1][ys] = obstacle # Si cette case représente un obstacle
    if xs+1 < hauteur :
        casesRemplies += 1
        if M[xs+1][ys] != obstacle :
            MatriceDistance[xs+1][ys] = 1
            CasesPre.append((xs+1,ys))
        else : MatriceDistance[xs+1][ys] = obstacle
    if ys-1 >= 0 :
        casesRemplies += 1
        if M[xs][ys-1] != obstacle :
            MatriceDistance[xs][ys-1] = 1
            CasesPre.append((xs,ys-1))
        else : MatriceDistance[xs][ys-1] = obstacle
    if ys+1 < largeur :
        casesRemplies += 1
        if M[xs][ys+1] != obstacle :
            MatriceDistance[xs][ys+1] = 1
            CasesPre.append((xs,ys+1))
        else : MatriceDistance[xs][ys+1] = obstacle
    # On itère ce procédé jusquà ce que toutes les cases soient remplies (casesRemplies < nombreCases) ou, si certaines cases ne peuvent pas être remplies, jusqu'à ce que l'on soit sûr qu'on ne peut plus remplir de cases (n < nombreCases)
    n=2
    while casesRemplies < nombreCases and n < nombreCases :
        Cases = []
        for I in CasesPre : # On regarde les cases voisines de chaque cases remplies à l'itération précédente
            x,y = I
            if x-1 >= 0 : # c'est à dire que cette case voisine existe bien
                if MatriceDistance[x-1][y] == -1 : # c'est à dire que cette case n'a pas encore été remplie
                    casesRemplies += 1
                    if M[x-1][y] == obstacle : MatriceDistance[x-1][y] = obstacle
                    else :
                        MatriceDistance[x-1][y] = n
                        Cases.append((x-1,y))
            if x+1 < hauteur : 
                if MatriceDistance[x+1][y] == -1 :
                    casesRemplies +=1
                    if M[x+1][y] == obstacle : MatriceDistance[x+1][y] = obstacle
                    else :
                        MatriceDistance[x+1][y] = n
                        Cases.append((x+1,y))
            if y-1 >= 0 :
                if MatriceDistance[x][y-1] == -1 :
                    casesRemplies +=1
                    if M[x][y-1] == obstacle : MatriceDistance[x][y-1] = obstacle
                    else :
                        MatriceDistance[x][y-1] = n
                        Cases.append((x,y-1))
            if y+1 < largeur :
                if MatriceDistance[x][y+1] == -1 :
                    casesRemplies +=1
                    if M[x][y+1] == obstacle : MatriceDistance[x][y+1] = obstacle
                    else :
                        MatriceDistance[x][y+1] = n
                        Cases.append((x,y+1))
        CasesPre = [Cases[i] for i in range(len(Cases))]
        n += 1
    CasesBloquees = [] # On mettra dans ce tableau toutes les cases à partir desquelles on ne peut pas atteindre la sortie (càd celles non remplies précedemment)
    # Maintenant que l'on a la matrice des distances, on peut remplir la matrice des déplacements.
    # Pour cela, à chaque case de la matrice réprésentative de la salle, on associe un tableau :
    #   - Vide si c'est un obstacle, ou que la case est bloquée
    #   - Comportant les coordonnées des cases voisines plus proches de la sortie, donc celles vers lesquelles on doit se déplacer pour rejoindre la sortie
    MatriceDeplacement = [[[] for j in range(largeur)] for i in range(hauteur)]
    for i in range(hauteur) :
        for j in range(largeur) :
            if M[i][j] != obstacle : # Si la case est un obstacle, on laisse vide le tableau
                if M[i][j] == -1 : CasesBloquees.append((i,j))
                else :
                    # On regarde tous les voisins pour voir lesquels correspondent
                    if i-1 >= 0 :
                        if MatriceDistance[i-1][j] == MatriceDistance[i][j] - 1 :
                            (MatriceDeplacement[i][j]).append((i-1,j))
                    if i+1 < hauteur :
                        if MatriceDistance[i+1][j] == MatriceDistance[i][j] - 1 :
                            (MatriceDeplacement[i][j]).append((i+1,j))
                    if j-1 >= 0 :
                        if MatriceDistance[i][j-1] == MatriceDistance[i][j] - 1 :
                            (MatriceDeplacement[i][j]).append((i,j-1))
                    if j+1 < largeur :
                        if MatriceDistance[i][j+1] == MatriceDistance[i][j] - 1 :
                            (MatriceDeplacement[i][j]).append((i,j+1))
    # Et c'est fini !
    return (MatriceDistance, MatriceDeplacement, CasesBloquees)
                    
   
   
   
                
def fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2) :
    # Cette fonction prend en entrée les matrices de distance et de déplacement pour deux différentes sorties, et renvoie la matrice de déplacement prenant en compte les deux sorties ainsi que le tableau des cases n'ayant accès à aucune sortie.
    hauteur = len(MatDist1)
    largeur = len(MatDist1[0])
    MatDep = [] # Matrice de déplacement finale
    CasesBloq = [] # Tableau des cases n'ayant accès à aucune sortie
    for i in range(hauteur) :
        MatDep.append([])
        for j in range(largeur) :
            (MatDep[i]).append([])
            if MatDep1[i][j] == [] : # C'est à dire qu'on ne peut pas atteindre la sortie 1 en partant de cette case, ou que la case est un obstacle
                if MatDep2[i][j] == [] :
                    CasesBloq.append((i,j))
                elif MatDist1[i][j] != 0 :
                    for dep in MatDep2[i][j] :
                        (MatDep[i][j]).append(dep)
            else :
                if MatDep2[i][j] == [] and MatDist2[i][j] != 0: # C'est à dire qu'on ne peut pas atteindre la sortie 2 en partant de cette case, ou que la case est un obstacle
                    for dep in MatDep1[i][j] :
                        (MatDep[i][j]).append(dep)
                else :
                    if MatDist1[i][j] < MatDist2[i][j] : # Il est plus rapide d'aller vers la sortie 1
                        for dep in MatDep1[i][j] :
                            (MatDep[i][j]).append(dep)
                    elif MatDist1[i][j] > MatDist2[i][j] : # Il est plus rapide d'aller vers la sortie 2
                        for dep in MatDep2[i][j] :
                            (MatDep[i][j]).append(dep)    
                    else : # Il est aussi rapide d'aller vers la sortie 1 que vers la sortie 2
                        Aux = []
                        for dep in MatDep1[i][j] :
                            Aux.append(dep)
                        for dep in MatDep2[i][j] :
                            Aux.append(dep)
                        for dep in list(set(Aux)) :
                            (MatDep[i][j]).append(dep)
    return(MatDep,CasesBloq)





def matPersonnes(M,Personnes) :
    # Cette fonction prend en entrée la matrice de la pièce, ainsi que le tableau des personnes et renvoie la Matrice des Personnes c-à-d une matrice ayant la taille de la pièce et pour coefficient : True si une personne est sur la case, False sinon.
    hauteur = len(M)
    largeur = len(M[0])
    MatPers = []
    for i in range(hauteur) :
        MatPers.append([])
        for j in range(largeur) :
            (MatPers[i]).append(False)
    for personne in Personnes : 
        x,y = personne[0]
        MatPers[x][y] = True
    return(MatPers)
    
    
def sitBloq(M,Personnes,CasesBloq) :
    # Cette fonction prend en entrée la matrice représentant la pièce, le tableau des personnes ainsi que la tableau des cases n'ayant pas accès à la sortie. Si une personne est sur une des cases bloquées, la fonction renvoie True, sinon elle renvoie False.
    MatPers = matPersonnes(M,Personnes)
    # Cette matrice permet de faciliter le fait de vérifier qu'une case bloquée n'a pas une personne dessus.
    for case in CasesBloq :
        x,y = case
        if MatPers[x][y]==1 :
            return(True)
    return(False)
    
    
    

def deplacement(MatDep,MatPers,Personnes,Sorties,Modif,nb_persos) :
# Cette procédure simule une étape de déplacement des personnes
# Modif[1] indique si un déplacement a eu lieu dans cette étape de déplacement
# Modif[0] indique si un déplacement a eu lieu dans ce temps
    for rang in range(len(Personnes)) :
        personne = Personnes[rang]
        if personne[1] and personne[2] :
            x,y = personne[0]
            Directions = MatDep[x][y]
            n = len(Directions)
            i = 0
            while i < n and personne[1] :
                x_dir,y_dir = Directions[i]
                if not(MatPers[x_dir][y_dir]) : # S'il n'y a personne sur la case vers laquelle la personne veut y aller
                    MatPers[x][y] = False # Elle quitte sa case actuelle
                    MatPers[x_dir][y_dir] = True # Elle arrive sur sa nouvelle case
                    Personnes[rang][0] = (x_dir,y_dir) # Ses coordonnées s'actualisent
                    Personnes[rang][1] = False # Il s'est déplacé ce tour-ci
                    Modif[1] = True
                    Modif[0] = True
                i = i + 1
            for sortie in Sorties :
                xs,ys = sortie
                if (x,y) == (xs,ys) :
                    Modif[1] = True
                    Modif[0] = True
                    Personnes[rang][2] = False # La personne quitte la matrice
                    MatPers[xs][ys] = False # La personne quitte la pièce, sa case devient vide 
                    nb_persos[0] = nb_persos[0] - 1 # Il y a une personne en moins dans la pièce             
                    



def methodeSimple(M,Personnes0,sortie1,sortie2,tmax) :
    """Prend en argument la matrice de la salle, le tableau initial des personnes, la position des deux sorties et un temps tmax. Renvoie le temps que met la salle à être évacuée s'il est plus petit que tmax, ou renvoie que le temps est dépassé. Renvoie également un message d'erreur en cas de situation non évacuable."""
    Personnes = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    MatDist1,MatDep1,CasesBloq1 = matDep(M,sortie1)
    MatDist2,MatDep2,CasesBloq2 = matDep(M,sortie2)
    MatDep,CasesBloq = fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2)
    if sitBloq(M,Personnes,CasesBloq) :
        return("situation bloquée")
    MatPers = matPersonnes(M,Personnes)
    nb_persos = [len(Personnes)] # Il s'agit du nombre de personnes encore dans la salle
    Modif = [True,True] # Le premier booléen indique si un action a été effectué durant cette période, le second pendant cette étape
    t = 0 # nombre d'étapes de temps effectuées
    while Modif[0] and nb_persos[0] != 0 and t < tmax :
        Modif[0] = False
        Modif[1] = True
        while Modif[1] :
            Modif[1] = False
            deplacement(MatDep,MatPers,Personnes,[sortie1,sortie2],Modif,nb_persos)
        t += 1
        for n in range(len(Personnes)) :
            Personnes[n][1] = True
    if nb_persos[0] == 0 :
        return(t)
    if t == tmax :
        return ("temps dépassé")
    return("situation bloquée")
    

    

def configPersonnes(M,Personnes0,sortie1,sortie2) :
    """Cette fonction teste pour une configuration de salle donnée plusieurs ordres du tableau des personnes pour voir si tous ces ordres ont le même temps d'évacuation : true s'ils ont tous le même temps, false sinon."""
    t_base = methodeSimple(M,Personnes0,sortie1,sortie2,100)
    nbPerso = len(Personnes0)
    longPerso = len(Personnes0[0])
    Temps = []
    verif = True
    for i in range(10000) :
        for k in range(nbPerso) :
            n = random.randint(0,nbPerso - 1)
            personneTemp = [Personnes0[k][j] for j in range(longPerso)]
            Personnes0[k] = [Personnes0[n][j] for j in range(longPerso)]
            Personnes0[n] = [personneTemp[j] for j in range(longPerso)]
        t = (methodeSimple(M,Personnes0,sortie1,sortie2,100))
        Temps.append(t)
        if t != t_base :
            verif = False
    return(verif)
            


def imMatTest(M,matPers) : 
    """Entrées : Matrices de la salle et des personnes.
    Sortie : Image représentant la salle dans cette situation (marron obstacle, blanc case vide, bleue personne"""
    L = len(M)
    l=len(M[0])
    image = np.zeros((10*L, 10*l, 3), dtype=np.uint8) #Chaque case correspond à une carré de 10px
    for i in range(L) :
        for j in range(l) :
            if M[i][j] == obstacle : #La case est un obstacle (marron)
                for k in range(10) :
                    for m in range(10) :
                        image[10*i+k][10*j+m] = (185,122,87)
            elif matPers[i][j] == True : #Il y a quelqu'un sur la case (bleu)
                for k in range(10) :
                    for m in range(10) :
                        image[10*i+k][10*j+m] = (0,0,255)
            else : #La case est vide (blanc)
                for k in range(10) :
                    for m in range(10) :
                        image[10*i+k][10*j+m] = (255,255,255)
    plt.imshow(image)
    plt.show()

#On veut maintenant renvoyer l'image correspondant

def imMat(M,matPers,sortie1,sortie2) : 
    """ Entrées : Matrices de la salle et des Personnes, position de sorties.
    Sortie : Image représentant la salle avec un bordure noire si pas de sortie et rouge si une sortie. Obstacles en marron, case libre en blanc, case occupée par une personne en bleue."""
    L = len(M)
    l=len(M[0])
    image = np.zeros((10*L+4, 10*l+4, 3), dtype=np.uint8) #Chaque case correspond à un carré de 10px et on rajoute une bordure
    x1,y1 = sortie1
    x2,y2 = sortie2
    # On colorie la bordure d'une sortie en rouge
    if x1==0 and y1 == 0 :
        for k in range(12) :
            image[k][0] = (255,0,0)
            image[k][1] = (255,0,0)
            image[0][k] = (255,0,0)
            image[1][k] = (255,0,0)
    elif x1==L-1 and y1 == 0 :
        for k in range(12) :
            image[-k-1][0] = (255,0,0)
            image[-k-1][1] = (255,0,0)
            image[-1][k] = (255,0,0)
            image[-2][k] = (255,0,0)
    elif x1==0 and y1 == l-1 :
        for k in range(12) :
            image[k][-1] = (255,0,0)
            image[k][-2] = (255,0,0)
            image[0][-k-1] = (255,0,0)
            image[1][-k-1] = (255,0,0)
    elif x1==L-1 and y1 == l-1 :
        for k in range(12) :
            image[-k-1][-1] = (255,0,0)
            image[-k-1][-2] = (255,0,0)
            image[-1][-k-1] = (255,0,0)
            image[-2][-k-1] = (255,0,0)
    elif x1 == 0 :
        col = y1 * 10 + 2
        for k in range(10) :
            image[0][k + col] = (255,0,0)
            image[1][k + col] = (255,0,0)
    elif x1 == L - 1 :
        col = y1 * 10 + 2
        for k in range(10) :
            image[-1][k + col] = (255,0,0)
            image[-2][k + col] = (255,0,0)   
    elif y1 == 0 :
        lig = x1 * 10 + 2
        for k in range(10) :
            image[k + lig][0] = (255,0,0)
            image[k + lig][1] = (255,0,0)
    else :
        lig = x1 * 10 + 2
        for k in range(10) :
            image[k + lig][-1] = (255,0,0)
            image[k + lig][-2] = (255,0,0)
    if x2==0 and y2 == 0 :
        for k in range(12) :
            image[k][0] = (255,0,0)
            image[k][1] = (255,0,0)
            image[0][k] = (255,0,0)
            image[1][k] = (255,0,0)
    elif x2==L-1 and y2 == 0 :
        for k in range(12) :
            image[-k-1][0] = (255,0,0)
            image[-k-1][1] = (255,0,0)
            image[-1][k] = (255,0,0)
            image[-2][k] = (255,0,0)
    elif x2==0 and y2 == l-1 :
        for k in range(12) :
            image[k][-1] = (255,0,0)
            image[k][-2] = (255,0,0)
            image[0][-k-1] = (255,0,0)
            image[1][-k-1] = (255,0,0)
    elif x2==L-1 and y2 == l-1 :
        for k in range(12) :
            image[-k-1][-1] = (255,0,0)
            image[-k-1][-2] = (255,0,0)
            image[-1][-k-1] = (255,0,0)
            image[-2][-k-1] = (255,0,0)
    elif x2 == 0 :
        col = y2 * 10 + 2
        for k in range(10) :
            image[0][k + col] = (255,0,0)
            image[1][k + col] = (255,0,0)
    elif x2 == L - 1 :
        col = y2 * 10 + 2
        for k in range(10) :
            image[-1][k + col] = (255,0,0)
            image[-2][k + col] = (255,0,0)   
    elif y2 == 0 :
        lig = x2 * 10 + 2
        for k in range(10) :
            image[k + lig][0] = (255,0,0)
            image[k + lig][1] = (255,0,0)
    else :
        lig = x2 * 10 + 2
        for k in range(10) :
            image[k + lig][-1] = (255,0,0)
            image[k + lig][-2] = (255,0,0)       
    for i in range(L) :
        for j in range(l) :
            if M[i][j] == obstacle : #La case est un obstacle (marron)
                for k in range(10) :
                    for m in range(10) :
                        image[10*i+k+2][10*j+m+2] = (185,122,87)
            elif matPers[i][j] == True : #Il y a quelqu'un sur la case (bleu)
                for k in range(10) :
                    for m in range(10) :
                        image[10*i+k+2][10*j+m+2] = (0,0,255)
            else : #La case est vide (blanc)
                for k in range(10) :
                    for m in range(10) :
                        image[10*i+k+2][10*j+m+2] = (255,255,255)
    #plt.imshow(image)
    #plt.show()
    return(image)
        

def anim(Images) :
    """Renvoie une animation correspondant à la succession des images du tableau d'entrée"""
    fig = plt.figure()
    ims = []
    for i in range(len(Images)):
        im = plt.imshow(Images[i], animated=True)
        ims.append([im])

    ani = animation.ArtistAnimation(fig, ims, interval=1000, blit=True,repeat_delay=3000)

    # ani.save('dynamic_images.mp4')
    plt.show()    
    
#On peut donc avoir un nouveau programme de méthode simple mais avec animation :

def methodeSimpleAnim(M,Personnes0,sortie1,sortie2,tmax) :
    """ Méthode simple pour une configuration donnée renvoyant l'animation correspondant à l'évacuation de la salle"""
    Personnes = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    MatDist1,MatDep1,CasesBloq1 = matDep(M,sortie1)
    MatDist2,MatDep2,CasesBloq2 = matDep(M,sortie2)
    MatDep,CasesBloq = fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2)
    Images = []
    if sitBloq(M,Personnes,CasesBloq) :
        return("situation bloquée")
    MatPers = matPersonnes(M,Personnes)
    nb_persos = [len(Personnes)] # Il s'agit du nombre de personnes encore dans la salle
    Modif = [True,True] # Le premier booléen indique si un action a été effectué durant cette période, le second pendant cette étape
    t = 0 # nombre d'étapes de temps effectuées
    Images.append(imMat(M,MatPers,sortie1,sortie2))
    while Modif[0] and nb_persos[0] != 0 and t < tmax :
        Modif[0] = False
        Modif[1] = True
        while Modif[1] :
            Modif[1] = False
            deplacement(MatDep,MatPers,Personnes,[sortie1,sortie2],Modif,nb_persos)
        t += 1
        Images.append(imMat(M,MatPers,sortie1,sortie2))
        for n in range(len(Personnes)) :
            Personnes[n][1] = True
    anim(Images)

def methodeSimpleAnimTest():
    """Test de la méthode simple sur la I211"""
    methodeSimpleAnim(I211,PersonnesI211,(0,11),(13,11),100)
    
# L'objectif est maintenant de comparer la simulation informatique réalisée avec la réalité. Pour cela nous devons créer une fonction indiquant le "degré" de différence entre deux matrices de personne. Pour cela, la fonction suivant donne le nombre de cases différentes entre deux matrices :

def comparaison(M1,M2) :
    '''Entrée : deux matrices de même taille
    Sortie : nombre de cases différentes entre les matrices'''
    k = 0
    for i in range(len(M1)) :
        for j in range(len(M1[0])) :
            if M1[i][j] != M2[i][j] :
                k += 1
    return(k)
    

# On veut maintenant faire cela sur tout un intervalle de temps :

def correlation(D1,D2) :
    '''Entrée : deux listes des matrices en fonction du temps
    Sortie : somme des comparaison de chaque matrices des listes'''
    t1,t2 = len(D1),len(D2)
    dt = t1 - t2
    if dt > 0 : # S'il y a plus de matrices dans D1, on rajoute des matrices nulles à D2 pour qu'ils aient la même taille
        Mnulle = [[0 for j in range(len(D[0][0][0]))] for i in range(len(D[0][0]))]
        for k in range (dt) :
            D2.append(Mnulle)
    if dt < 0 : # De même si D2 est plus grand que D1
        Mnulle = [[0 for j in range(len(D[0][0][0]))] for i in range(len(D[0][0]))]
        for k in range (- dt) :
            D1.append(Mnulle)
    nb_dif = 0
    for k in range(max(t1,t2)) :
        nb_dif += comparaison(D1[k],D2[k])
    return(nb_dif)
    
    
    
def lire_image() :
    # Cette fonction prend un tableau paint en entrée et renvoie la MatPers correspondante
    e = 10**(-4) # valeur permettant de faire des égalités entre float
    def egalite(a,b) : # égalité entre deux float
        if abs(a-b) < e :
            return(True)
        else : return (False)
    img = mpimg.imread("test1.png")
    lignes = 14
    colonnes = 12
    bord_haut = 3
    bord_bas =3
    bord_gau =3
    bord_dro =3
    l = len(img)
    m = len(img[0])
    taille_ligne = (m - bord_gau - bord_dro - colonnes + 1)//colonnes
    taille_colonne = (l - bord_haut - bord_bas - lignes + 1)//lignes
    M = [[False for k in range(colonnes)] for i in range(lignes)]
    for i in range(lignes) :
        for j in range(colonnes) :
            case1 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][0]
            case2 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][1]
            case3 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][2]
            if not( egalite(1.0,case1) and egalite(1.0,case2) and egalite(1.0,case3)) and not(egalite(0.72549021,case1) and egalite(0.47843137,case2) and egalite(0.34117,case3)) :
                M[i][j] = True #Si la case est ni blanche (1,1,1),vide, ni marron (0.72,0.47,0.34), obstacle, alors c'est qu'il y a quelqu'un sur cette case
    return(M)
    
def lire_image_numero(n) :
    '''La fonction renvoie la Matrice des Personnes correspondant à l'image tab numéro n'''
    e = 10**(-4) # valeur permettant de faire des égalités entre float
    def egalite(a,b) : # égalité entre deux float
        if abs(a-b) < e :
            return(True)
        else : return (False)
    img = mpimg.imread("tab" + str(n) +".png")
    lignes = 14
    colonnes = 12
    bord_haut = 3
    bord_bas =3
    bord_gau =3
    bord_dro =3
    l = len(img)
    m = len(img[0])
    taille_ligne = (m - bord_gau - bord_dro - colonnes + 1)//colonnes
    taille_colonne = (l - bord_haut - bord_bas - lignes + 1)//lignes
    M = [[False for k in range(colonnes)] for i in range(lignes)]
    for i in range(lignes) :
        for j in range(colonnes) :
            case1 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][0]
            case2 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][1]
            case3 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][2]
            if not( egalite(1.0,case1) and egalite(1.0,case2) and egalite(1.0,case3)) and not(egalite(0.72549021,case1) and egalite(0.47843137,case2) and egalite(0.34117,case3)) :
                M[i][j] = True #Si la case est ni blanche (1,1,1),vide, ni marron (0.72,0.47,0.34), obstacle, alors c'est qu'il y a quelqu'un sur cette case
    return(M)

def lire_evac(n) :
    '''Cette fonction renvoie la liste des matrices des personnes pour les tab allant de 0 à n'''
    return([lire_image_numero(k) for k in range(n+1)])
   
   
def evac_pasapas(M,Personnes0,sortie1,sortie2,MatDep,tmax) :
    '''Cette fonction renvoie la liste des MatPers à chaque étape de temps'''
    Personnes = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    MatPers = matPersonnes(M,Personnes)
    nb_persos = [len(Personnes)] # Il s'agit du nombre de personnes encore dans la salle
    Modif = [True,True] # Le premier booléen indique si un action a été effectué durant cette période, le second pendant cette étape
    t = 0 # nombre d'étapes de temps effectuées
    Evac = [MatPers]
    while Modif[0] and nb_persos[0] != 0 and t < tmax :
        Modif[0] = False
        Modif[1] = True
        while Modif[1] :
            Modif[1] = False
            deplacement(MatDep,MatPers,Personnes,[sortie1,sortie2],Modif,nb_persos)
        t += 1
        Evac.append(MatPers)
        for n in range(len(Personnes)) :
            Personnes[n][1] = True
    return(Evac)


def correl_evac_mult(D,M,sortie1,sortie2,Personnes0) :
    '''Cette fonction teste la corrélation de l'évacuation D avec les évacuations simulées pour différents ordres de la liste des Personnes, et renvoie l'ordre le plus corrélé'''
    MatDist1,MatDep1,CasesBloq1 = matDep(M,sortie1)
    MatDist2,MatDep2,CasesBloq2 = matDep(M,sortie2)
    MatDep,CasesBloq = fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2)
    nbPerso = len(Personnes0)
    longPerso = len(Personnes0[0])
    MeilleurOrdre = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    bestCor = correlation(D,evac_pasapas(M,Personnes0,sortie1,sortie2,MatDep,2*len(D)))
    for i in range(10000) :
        for k in range(nbPerso) :
            n = random.randint(0,nbPerso - 1)
            personneTemp = [Personnes0[k][j] for j in range(longPerso)]
            Personnes0[k] = [Personnes0[n][j] for j in range(longPerso)]
            Personnes0[n] = [personneTemp[j] for j in range(longPerso)]
        cor = correlation(D,evac_pasapas(M,Personnes0,sortie1,sortie2,MatDep,2*len(D)))
        if cor < bestCor :
            bestCor = cor
            MeilleurOrdre = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    return(MeilleurOrdre,bestCor)
    
def correl_evac_I211(D) :
    return(correl_evac_mult(D,I211,(0,11),(13,11),PersonnesI211))
   
def evacJeudi(n) :
    '''fonction toute prête pour la situation réelle de Jeudi. Enjoy Ewen du futur ;) '''
    D = lire_evac(n)
    return(correl_evac_I211(D))
   

def ImageToMat(nom_fichier) :
    '''Cette fonction prend en entrée un nom de fichier correspondant à une image-tableau de la salle et des personnes, et renvoie la matrice correspondant à la salle et aux personnes'''
    img = mpimg.imread(nom_fichier + ".png")
    e = 10**(-4) # valeur permettant de faire des égalités entre float
    def egalite(a,b) : # égalité entre deux float
        if abs(a-b) < e :
            return(True)
        else : return (False)
    lignes = 14
    colonnes = 12
    bord_haut = 3
    bord_bas =3
    bord_gau =3
    bord_dro =3
    l = len(img)
    m = len(img[0])
    taille_ligne = (m - bord_gau - bord_dro - colonnes + 1)//colonnes
    taille_colonne = (l - bord_haut - bord_bas - lignes + 1)//lignes
    M = [[1 for k in range(colonnes)] for i in range(lignes)]
    # Matrice de la salle
    MatPers = [[False for k in range(colonnes)] for i in range(lignes)] # Matrice des personnes
    for i in range(lignes) :
        for j in range(colonnes) :
            case1 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][0]
            case2 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][1]
            case3 = img[ i * taille_colonne + taille_colonne //2 + bord_gau][j* taille_ligne + taille_ligne //2 + bord_haut][2]
            if egalite(0.72549021,case1) and egalite(0.47843137,case2) and egalite(0.34117,case3) :
                M[i][j] = obstacle # Si la case est marron (0.72,0.47,0.34) alors il y a un obstacle
            elif not(egalite(1.0,case1) and egalite(1.0,case2) and egalite(1.0,case3)) :
                MatPers[i][j] = True # Si la case n'est ni marron ni blanche alors il y a une personne dessus
    return(M,MatPers)


def tempsImage(nom_fichier) :
    '''Cette fonction prend en entrée un nom de fichier correspondant à une image-tableau de la salle et des personnes, et renvoie le temps mis pour évacuer celle-ci'''
    M,MatPers = ImageToMat(nom_fichier)
    Personnes = fromMatPerstoPersonnes(MatPers)
    t = methodeSimple(M,Personnes,(0,11),(13,11),100)
    return(t)
    
def evacImage(nom_fichier) :
    '''Cette fonction prend en entrée un nom de fichier correspondant à une image-tableau de la salle et des personnes, et renvoie le temps mis pour évacuer celle-ci'''
    M,MatPers = ImageToMat(nom_fichier)
    Personnes = fromMatPerstoPersonnes(MatPers)
    methodeSimpleAnim(M,Personnes,(0,11),(13,11),100)
    return()
    
    
# On effectue plusieurs versions alternatives de la méthode
# Afin de savoir laquelle colle le mieux à la réalité

# Méthode alternative 1 :
# Les personnes avancent de deux cases à la fois quand il n'y a
# personne devant elles

def deplacementAlt1(MatDep,MatPers,Personnes,Sorties,Modif,nb_persos) :
    ''' Deplacement alternatif 1 : une personne avance de deux cases à la fois si elle le peut'''
    for rang in range(len(Personnes)) :
        personne = Personnes[rang]
        if personne[1] and personne[2] :
            x,y = personne[0]
            Directions = MatDep[x][y]
            n = len(Directions)
            i = 0
            while i < n and personne[1] :
                x_dir,y_dir = Directions[i]
                if not(MatPers[x_dir][y_dir]) : # S'il n'y a personne sur la case vers laquelle la personne veut y aller
                    MatPers[x][y] = False # Elle quitte sa case actuelle
                    MatPers[x_dir][y_dir] = True # Elle arrive sur sa nouvelle case
                    Personnes[rang][0] = (x_dir,y_dir) # Ses coordonnées s'actualisent
                    Personnes[rang][1] = False # Il s'est déplacé ce tour-ci
                    Modif[1] = True
                    Modif[0] = True
                    deuxiemeDep = False
                    i2 = 0
                    Directions2 = MatDep[x_dir][y_dir]
                    n2 = len(Directions2)
                    while i2 < n2 and not(deuxiemeDep) :
                        x_dir2,y_dir2 = Directions2[i2]
                        if not(MatPers[x_dir2][y_dir2]) :
                            MatPers[x_dir][y_dir] = False
                            MatPers[x_dir2][y_dir2] = True
                            Personnes[rang][0] =(x_dir2,y_dir2)
                            deuxiemeDep = True
                        i2 += 1
                i = i + 1
            for sortie in Sorties :
                xs,ys = sortie
                if (x,y) == (xs,ys) :
                    Modif[1] = True
                    Modif[0] = True
                    Personnes[rang][2] = False # La personne quitte la matrice
                    MatPers[xs][ys] = False # La personne quitte la pièce, sa case devient vide 
                    nb_persos[0] = nb_persos[0] - 1 # Il y a une personne en moins dans la pièce
                    
def methodeSimpleAlt1(M,Personnes0,sortie1,sortie2,tmax) :
    '''Alternative 1 : une personne avance de deux cases si elle le peut'''
    Personnes = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    MatDist1,MatDep1,CasesBloq1 = matDep(M,sortie1)
    MatDist2,MatDep2,CasesBloq2 = matDep(M,sortie2)
    MatDep,CasesBloq = fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2)
    if sitBloq(M,Personnes,CasesBloq) :
        return("situation bloquée")
    MatPers = matPersonnes(M,Personnes)
    nb_persos = [len(Personnes)] # Il s'agit du nombre de personnes encore dans la salle
    Modif = [True,True] # Le premier booléen indique si un action a été effectué durant cette période, le second pendant cette étape
    t = 0 # nombre d'étapes de temps effectuées
    while Modif[0] and nb_persos[0] != 0 and t < tmax :
        Modif[0] = False
        Modif[1] = True
        while Modif[1] :
            Modif[1] = False
            deplacementAlt1(MatDep,MatPers,Personnes,[sortie1,sortie2],Modif,nb_persos)
        t += 1
        for n in range(len(Personnes)) :
            Personnes[n][1] = True
    if nb_persos[0] == 0 :
        return(t)
    if t == tmax :
        return ("temps dépassé")
    return("situation bloquée")
    
def methodeSimpleAnimAlt1(M,Personnes0,sortie1,sortie2,tmax) :
    """Animation de la méthode alternative 1"""
    Personnes = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    MatDist1,MatDep1,CasesBloq1 = matDep(M,sortie1)
    MatDist2,MatDep2,CasesBloq2 = matDep(M,sortie2)
    MatDep,CasesBloq = fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2)
    Images = []
    if sitBloq(M,Personnes,CasesBloq) :
        return("situation bloquée")
    MatPers = matPersonnes(M,Personnes)
    nb_persos = [len(Personnes)] # Il s'agit du nombre de personnes encore dans la salle
    Modif = [True,True] # Le premier booléen indique si un action a été effectué durant cette période, le second pendant cette étape
    t = 0 # nombre d'étapes de temps effectuées
    Images.append(imMat(M,MatPers,sortie1,sortie2))
    while Modif[0] and nb_persos[0] != 0 and t < tmax :
        Modif[0] = False
        Modif[1] = True
        while Modif[1] :
            Modif[1] = False
            deplacementAlt1(MatDep,MatPers,Personnes,[sortie1,sortie2],Modif,nb_persos)
        t += 1
        Images.append(imMat(M,MatPers,sortie1,sortie2))
        for n in range(len(Personnes)) :
            Personnes[n][1] = True
    anim(Images)
    
def methodeSimpleAnimAlt1Test():
    """Test de la méthode alternative 1 sur la I211"""
    methodeSimpleAnimAlt1(I211,PersonnesI211,(0,11),(13,11),100)
    
def tempsImageAlt1(nom_fichier) :
    M,MatPers = ImageToMat(nom_fichier)
    Personnes = fromMatPerstoPersonnes(MatPers)
    t = methodeSimpleAlt1(M,Personnes,(0,11),(13,11),100)
    return(t)
    
def fromMatPerstoPersonnes(MatPers) :
    """Renvoie le tableau des personnes correspondant à la Matrice des personnes en entrée"""
    Personnes = []
    for i in range(len(MatPers)) :
        for j in range(len(MatPers[0])) :
            if MatPers[i][j] : Personnes.append([(i,j),True,True])
    return(Personnes)
    
def temps() :
    '''Cette fonction (à modifier pour chaque fonction renvoie le temps pris par chaque fonction'''
    debut = time.time()
    for k in range(1000) :
        a = methodeSimple(MatTest1,PersonnesTest1,(0,9),(11,9),100)
    fin = time.time()
    return((fin - debut)/1000)
        

def methodeSimpleMeilleur(M,Personnes0,sortie1,sortie2,tmax) :
    """Methode simple sauf qu'elle renvoie -1 si elle ne peut pas évacuer et le temps d'évacuation sinon"""
    Personnes = [[Personnes0[i][j] for j in range(len(Personnes0[0]))]for i in range(len(Personnes0))]
    MatDist1,MatDep1,CasesBloq1 = matDep(M,sortie1)
    MatDist2,MatDep2,CasesBloq2 = matDep(M,sortie2)
    MatDep,CasesBloq = fusionMatDep(MatDep1,MatDist1,MatDep2,MatDist2)
    if sitBloq(M,Personnes,CasesBloq) :
        #print("sitbloq")
        #print(CasesBloq)
        return(-1)
    MatPers = matPersonnes(M,Personnes)
    nb_persos = [len(Personnes)] # Il s'agit du nombre de personnes encore dans la salle
    Modif = [True,True] # Le premier booléen indique si un action a été effectué durant cette période, le second pendant cette étape
    t = 0 # nombre d'étapes de temps effectuées
    while Modif[0] and nb_persos[0] != 0 and t < tmax :
        Modif[0] = False
        Modif[1] = True
        while Modif[1] :
            Modif[1] = False
            deplacement(MatDep,MatPers,Personnes,[sortie1,sortie2],Modif,nb_persos)
        t += 1
        for n in range(len(Personnes)) :
            Personnes[n][1] = True
    if nb_persos[0] == 0 :
        #print("evac possible")
        return(t)
    if t == tmax :
        #print("tps trop long")
        return (-1)
    return(-1)
    
def meilleurtest() :
    """Renvoie les meilleurs configs de la I211"""
    tpsmin = -1
    Mat = [[0 for k in range(12)],[obstacle for k in range(12)],[0 for k in range(12)],[obstacle for k in range(12)],[0 for k in range(12)],[obstacle for k in range(12)],[0 for k in range(12)],[obstacle for k in range(12)],[0 for k in range(12)],[0 for k in range(12)],[0 for k in range(12)],[0 for k in range(12)],[0 for k in range(12)],[0 for k in range(12)]]
    depart = [0,1,2,3,4,5,6,7,8,9,10,11,24,25,26,27,28,29,30,31,32,33,34,35,48,49,50,51,52,53,54,55,56,57,58,59,72,73,74,75,76,77,78,79,80,81,82,83]
    tab = [0,1,2,3,4,5,6,7,8,9,10,11,24,25,26,27,28,29,30,31,32,33,34,35,48,49,50,51,52,53,54,55,56,57,58,59,72,73,74,75,76,77,78,79,80,81,82,83]
    fin = [47, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142]
    Personnes = [[(2, 0), True, True], [(2, 1), True, True], [(2, 2), True, True], [(2, 3), True
, True], [(2, 4), True, True], [(2, 5), True, True], [(2, 6), True, True], [(2, 
7), True, True], [(2, 8), True, True], [(2, 9), True, True], [(2, 10), True, True], [(2, 11), True, True], [(4, 0), True, True], [(4, 1), True, True], [(4, 2), 
True, True], [(4, 3), True, True], [(4, 4), True, True], [(4, 5), True, True], [
(4, 6), True, True], [(4, 7), True, True], [(4, 8), True, True], [(4, 9), True, 
True], [(4, 10), True, True], [(4, 11), True, True], [(6, 0), True, True], [(6, 
1), True, True], [(6, 2), True, True], [(6, 3), True, True], [(6, 4), True, True
], [(6, 5), True, True], [(6, 6), True, True], [(6, 7), True, True], [(6, 8), True, True], [(6, 9), True, True], [(6, 10), True, True], [(6, 11), True, True], [
(8, 0), True, True], [(8, 1), True, True], [(8, 2), True, True], [(8, 3), True, 
True], [(8, 4), True, True], [(8, 5), True, True], [(8, 6), True, True], [(8, 7)
, True, True], [(8, 8), True, True], [(8, 9), True, True], [(8, 10), True, True]
, [(8, 11), True, True]]
    bests = [Mat]
    compt = 0
    def suivant(i,coor = -1) :
        """Change tab, Mat et Personnes pour balayer toutes les configs possibles de la salle"""
        if coor == -1 : coor = tab[i]
        x = (coor+1) // 12 + 1
        y = (coor+1) % 12
        if i < 47 and coor + 1 == tab[i+1] : # Il y a une table sur la case suivante
            Mat[tab[i] // 12 + 1][tab[i] % 12] = 0
            tab[i] = depart[i]
            Mat[tab[i] // 12 + 1][tab[i] % 12] = obstacle
            Personnes[i] = [(depart[i] // 12 + 2,depart[i] % 12),True,True]
            suivant(i+1)
        elif Mat[x-1][y] == obstacle : #Il y a une personne sur la case suivante (i.e. il y a une table la ligne d'avant)
            suivant(i,coor + 1)
        elif Mat[x+1][y] == obstacle : # La personne associée à la table serait sur une autre table
            suivant(i,coor + 1)
        else : # Il n'y a rien sur la case suivante
            Mat[tab[i] // 12 + 1][tab[i] % 12] = 0
            tab[i] = coor + 1
            Mat[tab[i] // 12 + 1][tab[i] % 12] = obstacle
            Personnes[i] = [(tab[i] // 12 + 2,tab[i] % 12),True,True]
    comptmax = 3 * 10**32
    #Images = []
    while tab != fin and compt < comptmax :
        """tps = methodeSimpleMeilleur(Mat,Personnes,(0,11),(13,11),1000)
        if tps != -1 :
            if inferieur(tps,tpsmin) :
                tpsmin = tps
                bests = [Mat]
            elif tps == tpsmin :
                bests.append(Mat)"""
        suivant(0)
        compt += 1
        #Images.append(imMat(Mat,matPersonnes(Mat,Personnes),(0,11),(13,11)))
        print(tab)
        #print((sumtab(tab) - 19338091899991295482925055)/11144929410298132995434313683834805996748800)
    return(bests,tpsmin,compt)
    #anim(Images)


    
def meilleur(hauteur,largeur,nombre,sortie1,sortie2) :
    """Cette fonction prend en entrée la taille de la salle (hauteur*largeur), le nombre de tables/élèves ainsi que ses deux sorties, et renvoie les meilleures configs possibles de salle pour avoir l'évacuation la plus rapide possible"""
    tpsmin = -1
    Mat = [[0 for i in range(largeur)] for j in range(hauteur)]
    depart = []
    tab = []
    Personnes = []
    x1,y1 = sortie1
    x2,y2 = sortie2
    if (x1 == 0 or x1 > (2 + 2 * (nombre // largeur))) and (x2 == 0 or x2 > (2 + 2 * (nombre // largeur))) : # Les sorties ne se trouvent pas là où doivent être placées tables ou gens
        for k in range(nombre) :
            Mat[1 + 2 * (k // largeur)][k % largeur] = obstacle
            Personnes.append([(2 + 2 * (k // largeur),k % largeur),True,True])
            depart.append( k + largeur * (k//largeur))
            tab.append(k + largeur * (k//largeur))
    else :
        if x1 > 0 and x1 <= (2 + 2 * (nombre // largeur)) : coor1 = (x1 - 1)*largeur + y1
        else : coor1 = -1
        if x2 > 0 and x2 <= (2 + 2 * (nombre // largeur)) : coor2 = (x2 - 1)*largeur + y2
        else : coor2 = -1
        for k in range(nombre) :
            coor = k + largeur * (k//largeur)
            if inferieur(coor + largeur,coor1 - 1) :
                if inferieur(coor + largeur,coor2 - 1) :
                    Mat[1 + 2 * (k // largeur)][k % largeur] = obstacle
                    Personnes.append([(2 + 2 * (k // largeur),k % largeur),True,True])
                    depart.append( k + largeur * (k//largeur))
                    tab.append(k + largeur * (k//largeur))
                else :
                    Mat[1 + 2 * ((k+1) // largeur)][(k+1) % largeur] = obstacle
                    Personnes.append([(2 + 2 * ((k+1) // largeur),(k+1) % largeur),True,True])
                    depart.append( (k+1) + largeur * ((k+1)//largeur))
                    tab.append((k+1) + largeur * ((k+1)//largeur))
            else :
                if inferieur(coor + largeur,coor2 - 1) :
                    Mat[1 + 2 * ((k+1) // largeur)][(k+1) % largeur] = obstacle
                    Personnes.append([(2 + 2 * ((k+1) // largeur),(k+1) % largeur),True,True])
                    depart.append( (k+1) + largeur * ((k+1)//largeur))
                    tab.append((k+1) + largeur * ((k+1)//largeur))
                else :
                    Mat[1 + 2 * ((k+2) // largeur)][(k+2) % largeur] = obstacle
                    Personnes.append([(2 + 2 * ((k+2) // largeur),(k+2) % largeur),True,True])
                    depart.append( (k+2) + largeur * ((k+2)//largeur))
                    tab.append((k+2) + largeur * ((k+2)//largeur))            
    bests = [Mat]
    bests_pers = [Personnes]
    compt = 0
    comptmax = 10**10
    fin = [False]
    def suivant(i,coor = -1) :
        """Change tab, Mat et Personnes pour balayer toutes les configs possibles de la salle"""
        if coor == -1 : coor = tab[i]
        x = (coor+1) // largeur + 1
        y = (coor+1) % largeur
        if i < nombre - 1 and coor + 1 == tab[i+1] : # Il y a une table sur la case suivante
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = 0
            tab[i] = depart[i]
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = obstacle
            Personnes[i] = [((depart[i] // largeur) + 2,depart[i] % largeur),True,True]
            suivant(i+1)
        elif i == nombre - 1 and coor + 1 == largeur * (hauteur - 2) : # On a parcouru toutes les configs
            fin[0] = True
        elif Mat[x-1][y] == obstacle : #Il y a une personne sur la case suivante (i.e. il y a une table la ligne d'avant)
            suivant(i,coor + 1)
        elif Mat[x+1][y] == obstacle : # La personne associée à la table serait sur une autre table
            suivant(i,coor + 1)
        elif (x,y) == sortie1 or (x,y) == sortie2 or (x+1,y) == sortie1 or (x+1,y) == sortie2 : # La table ou sa personne associée serait sur une sortie
            suivant(i,coor + 1)
        else : # Il n'y a rien sur la case suivante
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = 0
            tab[i] = coor + 1
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = obstacle
            Personnes[i] = [(tab[i] // largeur + 2,tab[i] % largeur),True,True]
    while not(fin[0]) and compt < comptmax :
        tps = methodeSimpleMeilleur(Mat,Personnes,sortie1,sortie2,1000)
        if tps != -1 :
            if inferieur(tps,tpsmin) :
                tpsmin = tps
                #bests = [[tab[i] for i in range(len(tab))]]
                bests = [[[Mat[i][j] for j in range(largeur)] for i in range(hauteur)]]
                bests_pers = [Personnes[:]]
            elif tps == tpsmin :
                #bests.append([tab[i] for i in range(len(tab))])
                bests.append([[Mat[i][j] for j in range(largeur)] for i in range(hauteur)])
                bests_pers.append(Personnes[:])
        suivant(0)
        compt += 1
    #return(bests,tpsmin)
    return(bests,bests_pers)
    
def imagesMeilleur(hauteur,largeur,nombre,sortie1,sortie2) :
    """Renvoie l'animation des meilleurs configurations d'une telle salle"""
    Mats,Perss = meilleur(hauteur,largeur,nombre,sortie1,sortie2)
    Images = []
    for j in range(len(Mats)) :
        Mat = Mats[j]
        Pers = Perss[j]
        Mat_pers = matPersonnes(Mat,Pers)
        Images.append(imMat(Mat,Mat_pers,sortie1,sortie2))
    anim(Images)
    
    
def meilleur2sorties(Mat,Personnes) :
    """Renvoie pour une configuration de salle donnée, la meilleur configuration de deux sorties pour permettre l'évacuation la plus rapide possible"""
    sortie1,sortie2 = (0,1),(0,2)
    largeur = len(Mat[0])
    hauteur = len(Mat)
    tpsmin1 = methodeSimpleMeilleur(Mat,Personnes,(0,0),(0,1),1000)
    tpsmin2 = methodeSimpleMeilleur(Mat,Personnes,sortie1,sortie2,1000)
    if tpsmin1 == tpsmin2 :
        tpsmin = tpsmin1
        meilleursSorties = [((0,0),(0,1)),((0,1),(0,2))]
    elif inferieur(tpsmin1,tpsmin2) :
        tpsmin = tpsmin1
        meilleursSorties = [((0,0),(0,1))]
    else :
        tpsmin = tpsmin2
        meilleursSorties = [((0,1),(0,2))]
    while sortie1 != (hauteur - 1, largeur - 2) or sortie2 != (hauteur - 1, largeur - 1) :
        x1,y1 = sortie1
        x2,y2 = sortie2
        if y2 == y1 + 1 and x1 == x2 :
            if y2 < largeur - 1 :
                sortie1 = (0,1)
                sortie2 = (x2,y2 + 1)
            else :
                sortie1 = (0,1)
                sortie2 = (x2 + 1,0)
        elif y1 == largeur - 1 :
            if x2 == x1 + 1 and y2 == 0 : 
                if x2 == hauteur - 1 :
                    sortie1 = (0,1)
                    sortie2 = (x2,1)
                else : 
                    sortie1 = (0,1)
                    sortie2 = (x2,largeur - 1)
            else : sortie1 = (x1 + 1,0)
        elif y1 == 0 and x1 != hauteur - 1:
            if y2 == largeur - 1 and x2 == x1 : 
                sortie1 = (0,1)
                sortie2 = (x2 + 1,0)
            else : sortie1 = (x1,largeur - 1)
        else :
            sortie1 = (x1, y1 + 1)
        x1 , y1 = sortie1
        x2 , y2 = sortie2
        if Mat[x1][y1] == 1 and Mat[x1 - 1][y1] == 1 and Mat[x2][y2] == 1 and Mat[x2 - 1][y2] == 1 : #Les sorties ne sont ni sur une personne si sur une table
            tps = methodeSimpleMeilleur(Mat,Personnes,sortie1,sortie2,1000)
            if tps == tpsmin :
                meilleursSorties.append((sortie1,sortie2))
            elif inferieur(tps,tpsmin) :
                tpsmin = tps
                meilleursSorties = [(sortie1,sortie2)]
        print((sortie1,sortie2),tps)
    return(meilleursSorties,tpsmin)

     
def final2sorties(hauteur,largeur,nombre) :
    """Regarde toutes les configs de salle avec deux sorties pour une salle de taille hauteur*largeur """
    places0 = ((hauteur - 1)//2)*largeur
    sortie1,sortie2 = (0,1),(0,2)
    bests1,tpsmin1 = meilleur(hauteur,largeur,nombre,(0,0),(0,1))
    bests2,tpsmin2 = meilleur(hauteur,largeur,nombre,sortie1,sortie2)
    meilleures = []
    if tpsmin1 == tpsmin2 :
        tpsmin = tpsmin1
        meilleures.append((((0,0),(0,1)),bests1[:]))
        meilleures.append((((0,1),(0,2)),bests2[:]))
    elif inferieur(tpsmin1,tpsmin2) :
        tpsmin = tpsmin1
        meilleures.append((((0,0),(0,1)),bests1[:]))
    else :
        tpsmin = tpsmin2
        meilleures.append((((0,1),(0,2)),bests2))
    while sortie1 != (hauteur - 1, largeur - 2) or sortie2 != (hauteur - 1, largeur - 1) :
        places = places0
        x1,y1 = sortie1
        x2,y2 = sortie2
        if y2 == y1 + 1 and x1 == x2 :
            if y2 < largeur - 1 :
                sortie1 = (0,1)
                sortie2 = (x2,y2 + 1)
            else :
                sortie1 = (0,1)
                sortie2 = (x2 + 1,0)
        elif y1 == largeur - 1 :
            if x2 == x1 + 1 and y2 == 0 : 
                if x2 == hauteur - 1 :
                    sortie1 = (0,1)
                    sortie2 = (x2,1)
                else : 
                    sortie1 = (0,1)
                    sortie2 = (x2,largeur - 1)
            else : sortie1 = (x1 + 1,0)
        elif y1 == 0 and x1 != hauteur - 1:
            if y2 == largeur - 1 and x2 == x1 : 
                sortie1 = (0,1)
                sortie2 = (x2 + 1,0)
            else : sortie1 = (x1,largeur - 1)
        else :
            sortie1 = (x1, y1 + 1)
        x1 , y1 = sortie1
        x2 , y2 = sortie2
        if x1 > 0 : places = places - 1
        if x2 > 0 : places = places - 1
        if nombre <= places :
            bests,tps = meilleur(hauteur,largeur,nombre,sortie1,sortie2)
            if tps == tpsmin :
                meilleures.append(((sortie1,sortie2),bests[:]))
            elif inferieur(tps,tpsmin) :
                tpsmin = tps
                meilleures = [((sortie1,sortie2),bests[:])]
    return(meilleures,tpsmin)
    
def configSalle(hauteur,largeur,nombre) :
    """Teste toutes les config de salle à deux sorties (itère d'abord les configs de tables puis celles de sorties)"""
    tpsmin = -1
    Mat = [[0 for i in range(largeur)] for j in range(hauteur)]
    depart = []
    tab = []
    Personnes = []
    for k in range(nombre) :
        Mat[1 + 2 * (k // largeur)][k % largeur] = obstacle
        Personnes.append([(2 + 2 * (k // largeur),k % largeur),True,True])
        depart.append( k + largeur * (k//largeur))
        tab.append(k + largeur * (k//largeur))
    bests = []
    bests_pers = []
    compt = 0
    comptmax = 10**10
    fin = [False]
    def suivant(i,coor = -1) :
        """Change tab, Mat et Personnes pour balayer toutes les configs possibles de la salle"""
        if coor == -1 : coor = tab[i]
        x = (coor+1) // largeur + 1
        y = (coor+1) % largeur
        if i < nombre - 1 and coor + 1 == tab[i+1] : # Il y a une table sur la case suivante
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = 0
            tab[i] = depart[i]
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = obstacle
            Personnes[i] = [((depart[i] // largeur) + 2,depart[i] % largeur),True,True]
            suivant(i+1)
        elif i == nombre - 1 and coor + 1 == largeur * (hauteur - 2) : # On a parcouru toutes les configs
            fin[0] = True
        elif Mat[x-1][y] == obstacle : #Il y a une personne sur la case suivante (i.e. il y a une table la ligne d'avant)
            suivant(i,coor + 1)
        elif Mat[x+1][y] == obstacle : # La personne associée à la table serait sur une autre table
            suivant(i,coor + 1)
        else : # Il n'y a rien sur la case suivante
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = 0
            tab[i] = coor + 1
            Mat[tab[i] // largeur + 1][tab[i] % largeur] = obstacle
            Personnes[i] = [(tab[i] // largeur + 2,tab[i] % largeur),True,True]
    while not(fin[0]) and compt < comptmax :
        for sor1 in range(2*(hauteur + largeur - 2) - 1) :
            for sor2 in range(sor1 + 1,2*(hauteur + largeur - 2)) :
                if sor1 < largeur : sortie1 = (0,sor1)
                elif sor1 > largeur + 2 * hauteur - 4 : sortie1 = (hauteur - 1,sor1 - (largeur + 2 * hauteur - 4))
                else : sortie1 = ( 1 + (sor1 - largeur)//2,((sor1-largeur)%2)*(largeur - 1))
                if sor2 < largeur : sortie2 = (0,sor2)
                elif sor2 > largeur + 2 * hauteur - 4 : sortie2 = (hauteur - 1,sor2 - (largeur + 2 * hauteur - 4))
                else : sortie2 = ( 1 + (sor2 - largeur)//2,((sor2-largeur)%2)*(largeur - 1))
                x1,y1 = sortie1
                x2,y2 = sortie2
                if Mat[x1][y1] == 0 and (x1 == 0 or Mat[x1 - 1][y1] == 0) and Mat[x2][y2] == 0 and (x2 == 0 or Mat[x2 - 1][y2] == 0) :
                    tps = methodeSimpleMeilleur(Mat,Personnes,sortie1,sortie2,1000)
                    if tps != -1 :
                        if inferieur(tps,tpsmin) :
                            tpsmin = tps
                            #bests = [[tab[i] for i in range(len(tab))]]
                            bests = [(sortie1,sortie2,[[Mat[i][j] for j in range(largeur)] for i in range(hauteur)])]
                            #bests_pers = [Personnes[:]]
                        elif tps == tpsmin :
                            #bests.append([tab[i] for i in range(len(tab))])
                            bests.append((sortie1,sortie2,[[Mat[i][j] for j in range(largeur)] for i in range(hauteur)]))
                            #bests_pers.append(Personnes[:])
        suivant(0)
        compt += 1
    # Visionnage des images : 
    """Images = []
    for j in range(len(bests)) :
        sortie1,sortie2,Mat = bests[j]
        Mat_pers = [[False for k in range(largeur)] for l in range(hauteur)]
        for k in range(1,largeur) :
            for l in range(hauteur) :
                if Mat[k - 1][l] == 9 : Mat_pers[k][l] = True
        Images.append(imMat(Mat,Mat_pers,sortie1,sortie2))
        mpimg.imsave("evac_" + str(hauteur) + "x" + str(largeur) + "_" + str(j) + ".png",imMat(Mat,Mat_pers,sortie1,sortie2))
    anim(Images)"""
    return(bests,tpsmin)
    #return(bests,bests_pers)

    
    
        
def sortiesHasard(hauteur,largeur) :
    """Renvoie deux sorties au hasard de la salle longueur*largeur"""
    taille = 2 * hauteur + 2 * largeur - 5
    s1 = random.randint(0,taille)
    s2 = random.randint(0,taille - 1)
    if s2 >= s1 : s2 += 1
    if s1 < largeur : sortie1 = (0,s1)
    elif s1 > largeur + 2 * hauteur - 3 : sortie1 = (hauteur - 1, s1 - largeur - 2*hauteur + 3)
    else : sortie1 = ((s1 - largeur)//2 + 1,((s1 - largeur)%2)*(largeur - 1))     
    if s2 < largeur : sortie2 = (0,s2)
    elif s2 > largeur + 2 * hauteur - 3 : sortie2 = (hauteur - 1, s2 - largeur - 2*hauteur + 3)
    else : sortie2 = ((s2 - largeur)//2 + 1,((s2 - largeur)%2)*(largeur - 1))
    return(sortie1,sortie2)
       
def caseSalleToNbTab(hauteur,largeur,case) :
    x,y = case
    if x==0 : return(-1)
    else : return( (x-1)*largeur + y )
    
def nbTabToCaseSalle(hauteur,largeur,n) :
    def addCouples(c1,c2) :
        x1,y1 = c1
        x2,y2 = c2
        return((x1+x2,y1+y2))
    def aux(nb) :
        if nb < largeur : return((1,nb))
        else : return(addCouples(aux(nb - largeur),(1,0)))
    return(aux(n))
       
    
def salleHasard(n,m,nb_pers,sortie1,sortie2) :
    """Renvoie une salle au hasard de taille n*m, comprenant nb_pers élèves, avec les deux sorties renseignées, où toutes les tables sont tournées vers le tableau"""
    Mat_place = [[True for j in range(m)] for i in range(n)] #Places disponibles des élèves
    x1,y1 = sortie1
    x2,y2 = sortie2
    Mat_place[x1][y1] = False
    if x1 != n - 1 : Mat_place[x1 + 1][y1] = False
    Mat_place[x2][y2] = False
    if x2 != n - 1 : Mat_place[x2 + 1][y2] = False
    liste_pos = list(range((n-2)*m))
    random.shuffle(liste_pos)
    i,k = 0,0
    Personnes = []
    Mat = [[1 for j in range(m)] for i in range(n)]
    while k < nb_pers and i < (n-2) * m:
        pos = liste_pos[i]
        x = 2 + pos//m
        y = pos % m
        if Mat_place[x][y] :
            Personnes.append([(x,y),True,True])
            Mat[x-1][y] = obstacle
            k += 1
            Mat_place[x][y] = False
            if x != n - 1 : Mat_place[x+1][y] = False
            Mat_place[x-1][y] = False
        i += 1
    if k != nb_pers :
        print("PROBLEME A REGLER")
    else :
        return(Mat,Personnes)
        
def salleHasardTotal(n,m,nb_pers,sortie1,sortie2) :
    """Renvoie une salle au hasard de taille n*m, comprenant nb_pers élèves, avec les deux sorties renseignées, où les tables sont dans des directions aléatoires"""
    x1,y1 = sortie1
    x2,y2 = sortie2
    Place = [[[True for j in range(m)] for i in range(n)] for k in range(4)]
    # Places disponibles des tables, où Place[0] correspond à la matrice des places disponibles où la table est en haut et l'élève en bas
    # Place[1] : table en bas , Place[2] : table à gauche , Place[3] : table à droite
    # /!\ Ici ce sont les tables, pas les élèves, à l'inverse de la fonction précédente
    for j in range(m) :
        for k in range(4) :
            Place[k][0][j] = False
        Place[0][n-1][j] = False
        Place[1][1][j] = False
    for i in range(n) :
        Place[2][i][m-1] = False
        Place[3][i][0] = False
    # On prend en compte les sorties :
    for k in range(4) :
        Place[k][x1][y1] = False
        Place[k][x2][y2] = False
    if x1 != 0 :
        Place[0][x1 - 1][y1] = False
    if x2 != 0 :
        Place[0][x2 - 1][y2] = False
    if x1 != n-1 :
        Place[1][x1 + 1][y1] = False
    if x2 != n-1 :
        Place[1][x2 + 1][y2] = False
    if y1 != 0 :
        Place[2][x1][y1 - 1] = False
    if y2 != 0 :
        Place[2][x2][y2 - 1] = False
    if y1 != m-1 :
        Place[3][x1][y1 + 1] = False
    if y2 != m-1 :
        Place[3][x2][y2 + 1] = False
    nb_tables_placees = 0 
    Personnes = []
    Mat = [[1 for j in range(m)] for i in range(n)]
    Liste = []
    for k in range(4) :
        for u in range(n*m) :
            Liste.append((k,u))
    random.shuffle(Liste)
    i = 0
    while nb_tables_placees != nb_pers and i != 4 * n * m :
        k,pos = Liste[i]
        x = pos // m
        y = pos % m
        if Place[k][x][y] :
            nb_tables_placees += 1
            Mat[x][y] = obstacle
            for t in range(4) :
                Place[t][x][y] = False
            if x!= 0 :
                Place[0][x-1][y] = False
            if x!= n-1 :
                Place[1][x+1][y] = False
            if y != 0 :
                Place[2][x][y-1] = False
            if y != m-1 :
                Place[3][x][y+1] = False
            if k == 0 :
                Personnes.append([(x+1,y),True,True])
            if k == 1 :
                Personnes.append([(x-1,y),True,True])
            if k == 2 :
                Personnes.append([(x,y + 1),True,True])
            if k == 3 :
                Personnes.append([(x,y - 1),True,True])
        i += 1
    """image = imMat(Mat,matPersonnes(Mat,Personnes),sortie1,sortie2)
    plt.imshow(image)
    plt.show()"""
    if nb_tables_placees != nb_pers : print("Aie aie aie")
    else :
        return(Mat,Personnes)
    
    
    
def meilleurHasard(hauteur,largeur,nb_pers,n) :
    """Renvoie la meilleur config pour l'évacuation après tests de n configs aléatoires"""
    meilleurTps = -1
    gagnant = ([],0,0)
    for k in range(n) :
        sortie1,sortie2 = sortiesHasard(hauteur,largeur)
        M,Personnes = salleHasard(hauteur,largeur,nb_pers,sortie1,sortie2)
        tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,1000)
        if inferieur(tps,meilleurTps) : 
            gagnant = (copy_Matrice(M),sortie1,sortie2)
            meilleurTps = tps
    return(meilleurTps,gagnant)
    
def meilleurHasardCrit(n,m,nb_pers,critere,n_test) :
    """Renvoie la meilleur config pour le temps d'évacuation et le critère"""    
    meilleurTps = -1
    meilleurCrit = -1
    gagnant = ([],[],0,0)
    for k in range(n_test) :
        sortie1,sortie2 = sortiesHasard(n,m)
        M,Personnes = salleHasard(n,m,nb_pers,sortie1,sortie2)
        tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,1000)
        crit = critere(n,m,M)
        if meilleurTps == -1 or (tps != -1 and (tps < meilleurTps or (tps == meilleurTps and (meilleurCrit == -1 or crit < meilleurCrit)))): 
            M_gagnant = copy_Matrice(M)
            Personnes_gagnant = copy_Matrice(Personnes)
            gagnant = (M_gagnant,Personnes_gagnant,sortie1,sortie2)
            meilleurTps = tps
            meilleurCrit = crit
    Mg,Pg,s1,s2 = gagnant
    image = imMat(Mg,matPersonnes(Mg,Pg),s1,s2)
    plt.imshow(image)
    plt.show()
    return(meilleurTps,gagnant)
    
def meilleurHasard_avec_sorties(hauteur,largeur,nb_pers,sortie1,sortie2,n) :
    """Renvoie la meilleur config pour l'évacuation après tests de n configs aléatoires"""
    meilleurTps = -1
    gagnant = []
    tps_max =  2 * hauteur * largeur
    Temps = [0 for i in range(tps_max)]
    for k in range(n) :
        M,Personnes = salleHasard(hauteur,largeur,nb_pers,sortie1,sortie2)
        tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,tps_max)
        if tps == -1 : Temps[0] += 1
        else : Temps[tps] += 1
        if inferieur(tps,meilleurTps) : 
            gagnant = copy_Matrice(Personnes)
            meilleurTps = tps
    t = 2 * hauteur * largeur - 1
    while Temps[t] == 0 :
        del Temps[t]
        t = t - 1
    x = [(i+1) for i in range(len(Temps))]
    largeur = 0.5
    plt.bar(x, Temps, largeur, color='b' )
    Noms_barres =["infini"]
    for i in range(1,len(Temps)) : Noms_barres.append(str(i))
    pylab.xticks(x, Noms_barres, rotation=40)
    plt.title("Nombre de configurations pour chaque temps de sortie")
    plt.show()
    return(meilleurTps,gagnant)

def Stochastique(hauteur,largeur,nb_pers,sortie1,sortie2,n,critere,version = 0) :
    """Renvoie la meilleur config pour l'évacuation après tests de n configs aléatoires, avec le critere. version 0 correspond à des tables tournées vers le tableau, tandis que version 1 correspond à des tables tournées dans n'importe quel sens."""
    meilleurTps = -1
    meilleurCrit = -1
    Meilleur_Mat = []
    Meilleur_Pers =[]
    tps_max =  2 * hauteur * largeur
    Temps = [0 for i in range(tps_max)]
    for k in range(n) :
        if version == 0 : M,Personnes = salleHasard(hauteur,largeur,nb_pers,sortie1,sortie2)
        else : M,Personnes = salleHasardTotal(hauteur,largeur,nb_pers,sortie1,sortie2)
        tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,tps_max)
        crit = critere(hauteur,largeur,M)
        if tps == -1 : Temps[0] += 1
        else : Temps[tps] += 1
        if meilleurTps == -1 or (tps != -1 and(tps < meilleurTps or (tps == meilleurTps and (meilleurCrit == -1 or crit < meilleurCrit)))): 
            Meilleur_Pers = copy_Matrice(Personnes)
            Meilleur_Mat = copy_Matrice(M)
            meilleurTps = tps
    t = 2 * hauteur * largeur - 1
    while Temps[t] == 0 :
        del Temps[t]
        t = t - 1
    x = [(i+1) for i in range(len(Temps))]
    largeur = 0.5
    plt.subplot(1,2,1)
    plt.bar(x, Temps, largeur, color='b' )
    Noms_barres =["infini"]
    for i in range(1,len(Temps)) : Noms_barres.append(str(i))
    pylab.xticks(x, Noms_barres, rotation=40)
    plt.title("Nombre de configurations pour chaque temps de sortie")
    plt.subplot(1,2,2)
    image = imMat(Meilleur_Mat,matPersonnes(Meilleur_Mat,Meilleur_Pers),sortie1,sortie2)
    plt.imshow(image)
    plt.show()
    return(meilleurTps,Meilleur_Mat,Meilleur_Pers)
    
"""__________________ALGORITHME GENETIQUE__________________"""
    
def fusion(n,m,Personnes1,Personnes2,sortie1,sortie2) :
    """Effectue la fusion dans le cadre d'un algo génétique des deux configs, pour une salle de taille n*m"""
    nb = len(Personnes1)
    Personnes_fusion = []
    k = 0 #Nombre de  personnes dans Personnes_fusion
    t = 0 #Config dans laquelle on piochera la prochaine personne
    k1,k2 = 0,0 # rang dans les Personnes de chaque config auquel on est arrivé
    Mat_place = [[True for i in range(m)] for j in range(n)]
    x1,y1 = sortie1
    x2,y2 = sortie2
    Mat_place[x1][y1] = False
    if x1 != n - 1 : Mat_place[x1 + 1][y1] = False
    Mat_place[x2][y2] = False
    if x2 != n - 1 : Mat_place[x2 + 1][y2] = False
    while k < nb and (k1 < nb or k2 < nb): # On remplit Personnes_fusion avec Personnes1 et Personnes2
        if t == 0 :
            personne = Personnes1[k1][0:]
            k1 += 1
        else :
            personne = Personnes2[k2][0:]
            k2 += 1
        x,y = personne[0]
        if Mat_place[x][y] :
            Personnes_fusion.append(personne)
            k += 1
            Mat_place[x][y] = False
            if x != n - 1 : Mat_place[x+1][y] = False
            Mat_place[x-1][y] = False
            t = 1 - t
        if k1 == nb : t = 1
        elif k2 == nb : t = 0
    if k < nb : # Si ce n'est pas suffisant, on finit Personnes_fusion avec des tables aléatoires
        liste_pos = list(range((n-2)*m))
        random.shuffle(liste_pos)
        i = 0
        while k < nb and i < (n-2) * m:
            pos = liste_pos[i]
            x = 2 + pos//m
            y = pos % m
            if Mat_place[x][y] :
                Personnes_fusion.append([(x,y),True,True])
                k += 1
                Mat_place[x][y] = False
                if x != n - 1 : Mat_place[x+1][y] = False
                Mat_place[x-1][y] = False
            i += 1
    if k < nb : #On ne peut pas completer Personnes_fusion avec des tables aléatoires
        return(-1) # A compléter
    else : 
        return(Personnes_fusion)
        
def mutation(n,m,M,Personnes,sortie1,sortie2) :
    """ On effectue une mutation de Personnes (en modifiant Personnes et M), ie on change une personne par une nouvelle aléatoirement, dans une salle de taille n*m"""
    nb = len(Personnes)
    k = random.randrange(0,nb)
    Mat_place = [[True for i in range(m)] for j in range(n)]
    for i in range(nb) :
        if i != k :
            x,y = Personnes[i][0]
            Mat_place[x][y] = False
            if x != n - 1 : Mat_place[x+1][y] = False
            Mat_place[x-1][y] = False
    x1,y1 = sortie1
    x2,y2 = sortie2
    Mat_place[x1][y1] = False
    if x1 != n - 1 : Mat_place[x1 + 1][y1] = False
    Mat_place[x2][y2] = False
    if x2 != n - 1 : Mat_place[x2 + 1][y2] = False
    liste_pos = list(range((n-2)*m))
    random.shuffle(liste_pos)
    (x,y) = Personnes[k][0]
    M[x-1][y] = 1
    r = 0
    while r < (n-2)*m :
        pos = liste_pos[r]
        x = 2 + pos//m
        y = pos % m
        if Mat_place[x][y] :
            r = (n-1)*m
            Personnes[k][0] = (x,y)
            M[x-1][y] = obstacle
        r += 1
        
def personnesToSalle(n,m,Personnes):
    """On créé la matrice d'une salle de taille n*m dont les personnes initiales sont celles de Personnes"""
    Mat = [[1 for j in range(m)] for i in range(n)]
    for personne in Personnes :
        (x,y) = personne[0]
        Mat[x-1][y] = obstacle
    return(Mat)
    
def algo_gen(n,m,nb_pers,sortie1,sortie2,taille_pop,p,nb_iter) :
    """Effectue un algorithme génétique (avec sélection de type tournoi) sur une salle de taille n*m avec ces deux sorties, pour trouver une configuration de faible coût d'évacuation avec nb_pers personnes, avec une taille de population taille_pop, une probabilité de mutation p et un nombre d'itération nb_iter.
    /!\ nb_pers est divisible par 4"""
    Population = []
    Temps = []
    tmax = n*m + nb_pers
    tps_min = -1
    for k in range(taille_pop) :
        M,Personnes = salleHasard(n,m,nb_pers,sortie1,sortie2)
        Population.append((M,Personnes))
        tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,tmax)
        if inferieur(tps,tps_min) :
            tps_min = tps
            Pers_min = copy_Matrice(Personnes)
        Temps.append(tps)
    Liste_iter = [0]
    Liste_nb_evac = [nombre_evac_reussie(Temps)]
    Liste_min = [tps_min]
    Liste_moy = [moyenne(Temps)]
    Liste_var = [ecart_quadr(Temps,Liste_moy[0])]
    Liste_ecart_quadr_min = [ecart_quadr(Temps,tps_min)]
    for k in range(nb_iter) :
        newPop = []
        newTps  = []
        ordre = list(range(taille_pop))
        random.shuffle(ordre)
        for i in range(taille_pop//2) : # Sélection
            k1,k2 = ordre[2*i],ordre[2*i + 1]
            if inferieur(Temps[k1],Temps[k2]) :
                newPop.append(Population[k1])
                newTps.append(Temps[k1])
            else :
                newPop.append(Population[k2])
                newTps.append(Temps[k2])
        ordre = list(range(taille_pop // 2))
        random.shuffle(ordre)
        for i in range(taille_pop // 4) : # Croisements
            k1,k2 = ordre[2*i],ordre[2*i + 1]
            M1,Personnes1 = Population[k1]
            M2,Personnes2 = Population[k2]
            Pers1 = Personnes1[0:]
            Pers2 = Personnes2[0:]
            Pers1_inv = [Pers1[nb_pers - 1 - j] for j in range(nb_pers)]
            Pers2_inv = [Pers2[nb_pers - 1 - j] for j in range(nb_pers)]
            Fils1 = fusion(n,m,Pers1,Pers2,sortie1,sortie2)
            Fils2 = fusion(n,m,Pers1_inv,Pers2_inv,sortie1,sortie2)
            Mfils1 = personnesToSalle(n,m,Fils1)
            Mfils2 = personnesToSalle(n,m,Fils2)
            t_fils1 = methodeSimpleMeilleur(Mfils1,Fils1,sortie1,sortie2,tmax)
            if inferieur(t_fils1,tps_min) :
                tps_min = t_fils1
                Pers_min = Fils1[0:]
            t_fils2 = methodeSimpleMeilleur(Mfils2,Fils2,sortie1,sortie2,tmax)
            if inferieur(t_fils2,tps_min) :
                tps_min = t_fils2
                Pers_min = Fils2[0:]
            newPop.append((Mfils1,Fils1))
            newPop.append((Mfils1,Fils1))
            newTps.append(t_fils1)
            newTps.append(t_fils2)
        Population = []
        Temps = []
        for i in range(taille_pop) : # Mutations
            M,Personnes = newPop[i]
            tps = newTps[i]
            alea = random.random()
            if alea < p : # On effectue la mutation
                mutation(n,m,M,Personnes,sortie1,sortie2)
                tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,tmax)
                if inferieur(tps,tps_min) :
                    tps_min = tps
                    Pers_min = Personnes[0:]
            Population.append((M,Personnes))
            Temps.append(tps)
        Liste_iter.append(k+1)
        Liste_min.append(tps_min)
        Liste_nb_evac.append(nombre_evac_reussie(Temps))
        Liste_moy.append(moyenne(Temps))
        Liste_var.append(ecart_quadr(Temps,Liste_moy[k+1]))
        Liste_ecart_quadr_min.append(ecart_quadr(Temps,tps_min))
    plt.subplot(3,2,1)
    plt.title("Moyenne")
    plt.plot(Liste_iter,Liste_moy)
    plt.subplot(3,2,2)
    plt.title("Minimum")
    plt.plot(Liste_iter,Liste_min)
    plt.subplot(3,2,3)
    plt.title("Variance")
    plt.plot(Liste_iter,Liste_var)
    plt.subplot(3,2,4)
    plt.title("Ecart quadratique au minimum")
    plt.plot(Liste_iter,Liste_ecart_quadr_min)
    plt.subplot(3,2,5)
    plt.title("Nombre d'évacuations réussies")
    plt.plot(Liste_iter,Liste_nb_evac)
    plt.show()
    return(tps_min,Pers_min)
        
def algo_gen_crit(n,m,nb_pers,sortie1,sortie2,taille_pop,p,nb_iter,crit) :
    """Effectue un algorithme génétique (avec sélection de type tournoi avec le critère auxiliaire crit (par ex lignTab ou Connexes) sur une salle de taille n*m avec ces deux sorties, pour trouver une configuration de faible coût d'évacuation avec nb_pers personnes, avec une taille de population taille_pop, une probabilité de mutation p et un nombre d'itération nb_iter.
    /!\ nb_pers est divisible par 4"""
    Population = []
    Temps = []
    Criteres = []
    tmax = n*m + nb_pers
    tps_min = -1
    crit_min = -1
    for k in range(taille_pop) :
        M,Personnes = salleHasard(n,m,nb_pers,sortie1,sortie2)
        Population.append((M,Personnes))
        tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,tmax)
        cri = crit(n,m,M)
        if tps_min == -1 or (tps != -1 and tps < tps_min) or (tps == tps_min and (crit_min == -1 or cri < crit_min)):
            tps_min = tps
            Pers_min = Personnes[0:]
            Mmin = copy_Matrice(M)
            crit_min = cri
        Temps.append(tps)
        Criteres.append(cri)
    Liste_iter = [0]
    Liste_nb_evac = [nombre_evac_reussie(Temps)]
    Liste_min = [tps_min]
    Liste_moy = [moyenne(Temps)]
    Liste_var = [ecart_quadr(Temps,Liste_moy[0])]
    Liste_ecart_quadr_min = [ecart_quadr(Temps,tps_min)]
    Liste_crit_min = [crit_min]
    for k in range(nb_iter) :
        newPop = []
        newTps  = []
        newCrits = []
        ordre = list(range(taille_pop))
        random.shuffle(ordre)
        for i in range(taille_pop//2) : # Sélection
            k1,k2 = ordre[2*i],ordre[2*i + 1]
            if Temps[k2] == -1 or Temps[k1] < Temps[k2] or (Temps[k1] == Temps[k2] and Criteres[k1] < Criteres[k2]) :
                newPop.append(Population[k1])
                newTps.append(Temps[k1])
                newCrits.append(Criteres[k1])
            else :
                newPop.append(Population[k2])
                newTps.append(Temps[k2])
                newCrits.append(Criteres[k2])
        ordre = list(range(taille_pop // 2))
        random.shuffle(ordre)
        for i in range(taille_pop // 4) : # Croisements
            k1,k2 = ordre[2*i],ordre[2*i + 1]
            M1,Personnes1 = Population[k1]
            M2,Personnes2 = Population[k2]
            Pers1 = Personnes1[0:]
            Pers2 = Personnes2[0:]
            Pers1_inv = [Pers1[nb_pers - 1 - j] for j in range(nb_pers)]
            Pers2_inv = [Pers2[nb_pers - 1 - j] for j in range(nb_pers)]
            Fils1 = fusion(n,m,Pers1,Pers2,sortie1,sortie2)
            Fils2 = fusion(n,m,Pers1_inv,Pers2_inv,sortie1,sortie2)
            Mfils1 = personnesToSalle(n,m,Fils1)
            Mfils2 = personnesToSalle(n,m,Fils2)
            t_fils1 = methodeSimpleMeilleur(Mfils1,Fils1,sortie1,sortie2,tmax)
            crit_fils1 = crit(n,m,Mfils1)
            crit_fils2 = crit(n,m,Mfils2)
            if tps_min == -1 or (t_fils1 != -1 and t_fils1 < tps_min) or (t_fils1 == tps_min and crit_fils1 < crit_min) :
                tps_min = t_fils1
                Pers_min = Fils1[0:]
                Mmin = copy_Matrice(Mfils1)
                crit_min = crit_fils1
            t_fils2 = methodeSimpleMeilleur(Mfils2,Fils2,sortie1,sortie2,tmax)
            if tps_min == -1 or (t_fils2 != -1 and t_fils2 < tps_min) or (t_fils2 == tps_min and crit_fils2 < crit_min) :
                tps_min = t_fils2
                Pers_min = Fils2[0:]
                Mmin = copy_Matrice(Mfils2)
                crit_min = crit_fils2
            newPop.append((Mfils1,Fils1))
            newPop.append((Mfils1,Fils1))
            newTps.append(t_fils1)
            newTps.append(t_fils2)
            newCrits.append(crit_fils1)
            newCrits.append(crit_fils2)
        Population = []
        Temps = []
        Min_mute = True
        for i in range(taille_pop) : # Mutations
            M,Personnes = newPop[i]
            tps = newTps[i]
            cri = newCrits[i]
            alea = random.random()
            if alea < p : # On effectue la mutation
                if Min_mute and tps == tps_min and cri == crit_min : # On conserve la meilleure configuration (ou une des meilleures)
                    Min_mute = False
                else :
                    mutation(n,m,M,Personnes,sortie1,sortie2)
                    tps = methodeSimpleMeilleur(M,Personnes,sortie1,sortie2,tmax)
                    cri = crit(n,m,M)
                    if tps_min == -1 or (tps != -1 and tps < tps_min) or (tps == tps_min and cri < crit_min) :
                        tps_min = tps
                        Pers_min = copy_Matrice(Personnes)
                        Mmin = copy_Matrice(M)
                        crit_min = cri
            Population.append((M,Personnes))
            Temps.append(tps)
            Criteres.append(cri)
        Liste_iter.append(k+1)
        Liste_min.append(tps_min)
        Liste_nb_evac.append(nombre_evac_reussie(Temps))
        Liste_moy.append(moyenne(Temps))
        Liste_var.append(ecart_quadr(Temps,Liste_moy[k+1]))
        Liste_ecart_quadr_min.append(ecart_quadr(Temps,tps_min))
        Liste_crit_min.append(crit_min)
    Im = imMat(Mmin,matPersonnes(Mmin,Pers_min),sortie1,sortie2)
    plt.subplot(3,2,1)
    plt.title("Moyenne")
    plt.plot(Liste_iter,Liste_moy)
    plt.subplot(3,2,2)
    plt.title("Minimum")
    plt.plot(Liste_iter,Liste_min)
    plt.subplot(3,2,5)
    #plt.title("Variance")
    #plt.plot(Liste_iter,Liste_var)
    plt.imshow(Im)
    plt.subplot(3,2,4)
    plt.title("Ecart quadratique au minimum")
    plt.plot(Liste_iter,Liste_ecart_quadr_min)
    plt.subplot(3,2,3)
    plt.title("Nombre d'évacuations réussies")
    plt.plot(Liste_iter,Liste_nb_evac)
    plt.subplot(3,2,6)
    #plt.imshow(Im)
    plt.title("Minimum du critère")
    plt.plot(Liste_iter,Liste_crit_min)
    plt.show()
    return(tps_min,Pers_min)

G = [[1,1,1,1,1,1,1,1,1,1,1,1],
[1,obstacle,obstacle,obstacle,1,obstacle,obstacle,obstacle,obstacle,obstacle,obstacle,1],
[obstacle,1,1,1,1,1,1,1,1,1,1,1],
[1,1,obstacle,1,1,obstacle,obstacle,1,obstacle,obstacle,1,1],
[obstacle,1,1,1,1,1,1,1,1,1,1,obstacle],
[1,obstacle,1,1,1,obstacle,obstacle,obstacle,obstacle,1,1,1],
[1,1,1,1,1,1,1,1,1,obstacle,1,1],
[1,obstacle,obstacle,obstacle,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,obstacle,obstacle,1,obstacle,obstacle],
[1,1,obstacle,1,1,1,obstacle,1,1,1,1,1],
[obstacle,obstacle,1,1,1,obstacle,1,1,obstacle,obstacle,obstacle,obstacle],
[1,1,1,obstacle,obstacle,1,obstacle,1,1,1,1,1],
[obstacle,obstacle,1,1,1,1,1,obstacle,obstacle,obstacle,obstacle,1],
[1,1,1,1,1,1,1,1,1,1,1,1]]

def matToPers(M) :
    Personnes = []
    for i in range(len(M)) :
        for j in range(len(M[0])) :
            if M[i][j] == obstacle :
                Personnes.append([(i+1,j),True,True])
    return(Personnes)

def variation(M,Personnes,n,m,s1,s2,nb_iter,crit) :
    tmax = 2*n*m
    temps = methodeSimpleMeilleur(M,Personnes,s1,s2,tmax)
    critere = crit(n,m,M)
    M1,Personnes1 = copy_Matrice(M),copy_Matrice(Personnes)
    for k in range(nb_iter) :
        M2,Personnes2 = copy_Matrice(M1),copy_Matrice(Personnes1)
        mutation(n,m,M2,Personnes2,s1,s2)
        cri = crit(n,m,M2)
        if methodeSimpleMeilleur(M2,Personnes2,s1,s2,tmax) == temps and cri < critere :
            critere = cri
            M1 = copy_Matrice(M2)
            Personnes1 = copy_Matrice(Personnes2)
    return(critere,M1,Personnes1)
        
# CRITERES AUXILIAIRES


def lignTab(n,m,M) :
    """Renvoie le nombre de lignes contenant des tables, avec une salle n*m"""
    nb_lignes = 0
    for ligne in M :
        table_sur_ligne = False
        for case in ligne :
            if case == obstacle :
                table_sur_ligne = True
        if table_sur_ligne :
            nb_lignes += 1
    return(nb_lignes)
            

def Connexes(n,m,M) :
    """ Renvoie le nombre de parties connexes de tables de M, i.e. le nombre de "groupe de tables collées" """
    if n*m > 1000 : sys.setrecursionlimit(n*m)
    M1 = copy_Matrice(M) # On créé une nouvelle matrice de salle que l'on pourra modifier
    nb = 0
    
    def partieConnexe(i,j) :
        """ Supprime toutes les tables de la partie connexe de (i,j) dans M1"""
        M1[i][j] = 1
        if M1[i-1][j] == obstacle :
            partieConnexe(i-1,j)
        if i < n - 1 and M1[i+1][j] == obstacle :
            partieConnexe(i+1,j)
        if j > 0 and M1[i][j-1] == obstacle :
            partieConnexe(i,j-1)
        if j < m - 1 and M1[i][j+1] == obstacle :
            partieConnexe(i,j+1)
    
    for i in range(n) :
        for j in range(m) :
            if M1[i][j] == obstacle :
                nb += 1
                partieConnexe(i,j)
    return(nb)
                
    

# UTILITAIRES

def nombre_evac_reussie(liste) :
    nb = 0
    for t in liste :
        if t != -1 : nb += 1
    return(nb)

def moyenne(liste) :
    moy = 0
    n = 0
    for t in liste :
        if t != -1 :
            n += 1
            moy += t
    if n !=0 : return(moy/n)
    else : return(0)

def ecart_quadr(liste,val) :
    ecart = 0
    n = 0
    for t in liste :
        if t != -1 :
            n += 1
            ecart += (t - val)**2
    if n != 0 : return(ecart/n)
    else : return(0)
    
def presCrois(n,m,nb_pers,sortie1,sortie2) :
    """Renvoie les images de deux parents et deux enfants"""
    M1,Personnes1 = salleHasard(n,m,nb_pers,sortie1,sortie2)
    M2,Personnes2 = salleHasard(n,m,nb_pers,sortie1,sortie2)
    Pers1 = Personnes1[0:]
    Pers2 = Personnes2[0:]
    Pers1_inv = [Pers1[nb_pers - 1 - j] for j in range(nb_pers)]
    Pers2_inv = [Pers2[nb_pers - 1 - j] for j in range(nb_pers)]
    Fils1 = fusion(n,m,Pers1,Pers2,sortie1,sortie2)
    Fils2 = fusion(n,m,Pers1_inv,Pers2_inv,sortie1,sortie2)
    Mfils1 = personnesToSalle(n,m,Fils1)
    Mfils2 = personnesToSalle(n,m,Fils2)
    P1 = imMat(M1,matPersonnes(M1,Personnes1),sortie1,sortie2)
    P2 = imMat(M2,matPersonnes(M2,Personnes2),sortie1,sortie2)
    F1 = imMat(Mfils1,matPersonnes(Mfils1,Fils1),sortie1,sortie2)
    F2 = imMat(Mfils2,matPersonnes(Mfils2,Fils2),sortie1,sortie2)
    plt.imshow(P1)
    plt.savefig("Parent1.png")
    plt.imshow(P2)
    plt.savefig("Parent2.png")
    plt.imshow(F1)
    plt.savefig("Fils1.png")
    plt.imshow(F2)
    plt.savefig("Fils2.png")

def aide(Personne) :
    M = [[1 for j in range(12)] for i in range(14)]
    for pers in Personnes :
        x,y = pers[0]
        M[x-1][y] = obstacle
    methodeSimpleAnim(M,Personnes,(0,11),(13,11),100)
    
def copy_Matrice(M) :
    """Renvoie une matrice copie de M"""
    n,m = len(M),len(M[0])
    M1 = [[0 for i in range(m)] for j in range(n)]
    for i in range(n) :
        for j in range(m) :
            M1[i][j] = M[i][j]
    return(M1)

def inferieur(a,b) : 
    """True si a < b, False sinon. En comptant -1 comme +infini"""
    if b <= -1 : return(True)
    if a == -1 : return(False)
    if a < b : return(True)
    else : return(False)
    
        